Yii2 Barcode Generator
======================
Gerador de Barcode para o Yii

Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist bandodebits/yii2-bdb-barcode "*"
```

or add

```
"bandodebits/yii2-bdb-barcode": "*"
```

to the require section of your `composer.json` file.


Usage
-----

Once the extension is installed, simply use it in your code by  :

```php
<?= \bdb\barcode\AutoloadExample::widget(); ?>```